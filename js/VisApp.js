
/**
 * VISUALIZATION APP
 *
 *
 * THIS IS THE MAIN 'CONTAINER OBJECT' FOR THE ENTIRE APP. ALL VISUALIZATIONS CALL THIS OBJECT!
 * IT IS THE PARENT FOR ALL SHARED VARIABLES
 *
 * @param url
 * @param callback
 * @constructor
 */

var VisualizationApp = function (url, callback) {


    var outerLayout, leftLayout, rightLayout;

    var selectorEnum = {
        "COUNTRY": "#ffd700",
        "RANGE": "green",
        "YEAR": "red"
    };

    var typeEnum = {
        "Bubble Chart": DottedChart,
        "Bar Chart": Barchart,
        "Pie Chart": PieChart,
        "Map": Map,
        "Disable": Visualization
    };


    // load the external data


    var dsv = d3.dsv(",", "text/plain");

    queue()
        .defer(dsv, url)
        .defer(d3.json, "data/countries.json")
        .await(ready);

    function ready(error, data, topology) {

        if (error) {
            console.log(error);
            alert('something went wrong with loading the data');
        }

        var validViews = ["#left-top", "#left-bottom", "#right-top", "#right-bottom"];

        var selectableCountries = [];
        var resultData = {};
        var endData = [];
        var startYear = 9999;
        var endYear = -9999;
        var series = {};

        data.forEach(function (d) {
            var sCode = d["Series Code"],
                cCode = d["Country Code"],
                cName = d["Country Name"];

            if(cName) {

                // compile a list of the different data attributes with their descriptions
                if (!series[sCode]) series[sCode] = {'name' : d["Series Name"], 'min': 999999999999999999999, 'max':-999999999999999999999};

                // create the country if it doesn't already exist
                if (!resultData[cCode]) {
                    if (selectableCountries.indexOf(cName) == -1)
                        selectableCountries.push(cName);
                    resultData[cCode] = {'country': cName};
                }

                $.each(d, function (key, value) {

                    var year = parseInt(key);
                    // if the key is one of the year entries, do the following:
                    if (typeof(year) == "number" && year < 2015) {
                        if (year > endYear) endYear = year;
                        if (year < startYear) startYear = year;
                        if (!resultData[cCode]["" + year]) resultData[cCode]["" + year] = {};

                        if(!value || value.length === 0) value = "N/A";
                        else{
                            if(+value > +series[sCode].max) series[sCode].max = +value;
                            if(+value < +series[sCode].min) series[sCode].min = +value;
                        }
                        resultData[cCode]["" + year][sCode] = value;
                    }
                });
            }
        });


        var countries = topojson.feature(topology, topology.objects.subcountries);

        countries.features.forEach(function (d){


            var cCode = d.properties.iso_a3;

            if(!resultData[cCode]) {
                // If the country is not found using the iso_a3 code, try again with the wb_a3 code
                cCode = d.properties.wb_a3;
            }

            if(resultData[cCode]){
                resultData[cCode]["continent"] = d.properties.continent;
                resultData[cCode]["geometry"] = d.geometry;
                resultData[cCode]["id"] = d.id;
            }
            else{
                // If neither of those work, the country doesn't exist in the dataset and we also do not visualize it
                // console.log('country failed: ' + d.properties.name + ' with ISO : ' + d.id);
            }
        });


        $.each(resultData, function(key, value){
            var res = $.extend({}, value);
            endData.push(res)
        });

        data = endData;

        var Vis = {
            views: {},

            // Set the variables here:
            selector: selectorEnum.COUNTRY,
            selectorEnum: selectorEnum,
            typeEnum: typeEnum,
            selectableCountries: selectableCountries.slice(),
            selected: selectableCountries,
            focused: [],
            startYear: startYear,
            endYear: endYear,
            selectedYear: startYear,
            hover: null,
            data: data,
            svg: (d3.select(".main-view-container")
                .append('svg')),
            validViews : validViews,
            defaultSeries : "IT.NET.USER.P2",
            series : series,


            // Set the exposed functions here
            initialize_viewports: function () {

                outerLayout = $('.view-container').layout({
                    center__paneSelector: ".outer-left"
                    , east__paneSelector: ".outer-right"
                    , east__size: .50
                    , spacing_open: 8 // ALL panes
                    , spacing_closed: 12 // ALL panes
                    , north__spacing_open: 0
                    , south__spacing_open: 0
                    , center__onresize: view_resize
                });

                leftLayout = $('div.outer-left').layout({
                    center__paneSelector: ".inner-left-top"
                    , south__paneSelector: ".inner-left-bottom"
                    , center__size: .50
                    , south__size: .50
                    , spacing_open: 8 // ALL panes
                    , spacing_closed: 12 // ALL panes
                    , center__onresize: view_resize
                });

                rightLayout = $('div.outer-right').layout({
                    center__paneSelector: ".inner-right-top"
                    , south__paneSelector: ".inner-right-bottom"
                    , center__size: .50
                    , south__size: .50
                    , spacing_open: 8 // ALL panes
                    , spacing_closed: 12 // ALL panes
                    , center__onresize: view_resize
                });


                this.outerLayout = outerLayout;
                this.leftLayout = leftLayout;
                this.rightLayout = rightLayout;
            },
            reset: function () {

                for (var v in Vis.views) {
                    if (Vis.views.hasOwnProperty(v)) {
                        Vis.views[v].reset();
                        delete Vis.views[v];
                    }
                }
                Vis.selected = selectableCountries.slice();
                Vis.selectedYear = startYear;
                Vis.focused = [];

                outerLayout.sizePane('east', 0.5);
                leftLayout.sizePane('south', 0.5);
                rightLayout.sizePane('south', 0.5);
                leftLayout.show('south', leftLayout.southState);
                rightLayout.show('south', rightLayout.southState);

            },
            update: function () {
                updateFocusable();
                for (var v in Vis.views) {
                    if (Vis.views.hasOwnProperty(v))
                        Vis.views[v].update();
                }
            },
            view_resize: view_resize,
            updateSelector : function(){
                Vis.data.forEach(function(d, i){
                    var newVal = (+d[Vis.selectedYear][Vis.defaultSeries]);
                    $("." + i + "-selector-val")
                        .html(newVal.toFixed(2))
                        .parent().attr('value', newVal)

                });
            },
            selectYear: function(year){
                year = year || this.selectedYear;
                this.selectedYear = year;
                Vis.yearSelector.slider("value", this.selectedYear);
                this.updateSelector();
                for (var v in Vis.views){
                    if(Vis.views.hasOwnProperty(v)) {
                        Vis.views[v].selectYear(year);
                    }
                }
            },

            setView : function(i, vis) {

                if (validViews.indexOf(i) == -1) return false;

                d3.select(i).selectAll("*").remove();
                Vis.views[i] = (new vis(i, Vis));

            }
        };

        function view_resize() {
            for (var v in Vis.views) {
                if (Vis.views.hasOwnProperty(v))
                    Vis.views[v].resize();
            }

            if ($(window).width() < 1000) {
                if (!outerLayout.east.state.isHidden) outerLayout.eastState = !outerLayout.east.state.isClosed;
                outerLayout.hide('east');
            }

            else {
                outerLayout.show('east', outerLayout.eastState);
            }

            if ($(window).height() < 800) {
                if (!leftLayout.south.state.isHidden) leftLayout.southState = !leftLayout.south.state.isClosed;
                if (!rightLayout.south.state.isHidden) rightLayout.southState = !rightLayout.south.state.isClosed;
                leftLayout.hide('south');
                rightLayout.hide('south');
            }
            else {
                leftLayout.show('south', leftLayout.southState);
                rightLayout.show('south', rightLayout.southState);
            }
        }

        Vis.initialize_viewports();
        return callback(Vis);
    };
};