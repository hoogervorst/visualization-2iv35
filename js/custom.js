/**
 * Created by Stijn on 30-1-2015.
 */
var Vis;

$(document).ready(function () {

    $.extend($.fn.disableTextSelect = function() {
        return this.each(function(){
            $(this).mousedown(function(){return false;});
        });
    });

    /*
     Load the rest of the visualization  library
     */

    var script_arr = [
        '../VisApp.js',
        'Visualization.js',
        'PieChart.js',
        'Barchart.js',
        'Map.js',
        'DottedChart.js'
    ];

    $.getMultiScripts = function (arr, path) {
        var _arr = $.map(arr, function (scr) {
            return $.getScript((path || "") + scr);
        });

        _arr.push($.Deferred(function (deferred) {
            $(deferred.resolve);
        }));

        return $.when.apply($, _arr);
    };

    $.getMultiScripts(script_arr, 'js/visualizations/').done(initializeApp);


});


var continents = {"Asia":"asia", "Europe":"europe", "Africa":"africa", "South America":"s-america", "Oceania":"oceania", "North America":"n-america"};

var initializeApp = function () {
    new VisualizationApp("data/Data_Extract_From_World_Development_Indicators_Data.csv", function (Vis) {

        // wait for visualization to finish loading

        this.Vis = Vis;
        $('.viewpane>*').disableTextSelect(); //No selection on doubleclick on center view



        /* Set the functions here */
        Vis.view_resize();
        $(window).resize(function(){
            set_country_selector_overlay_height();
            Vis.view_resize();
        });

        Vis.countryFocusSelector = '#country-focus-list';
        Vis.yearRangeSlider = '#year-slider';

        initialize_subroutine();
        initialize_buttons();
        initialize_country_selector();

    });
};


function toggleClick (val, $inp, state, holdUpdate){
    var idx;
    if (( idx = Vis.focused.indexOf( val ) ) > -1 ) {
        if(state == 1) return;
        Vis.focused.splice( idx, 1 );
        if($inp) $inp.prop( 'checked', false );
    } else {
        if(state == -1) return;
        Vis.focused.push( val );
        if($inp) $inp.prop( 'checked', true );
    }

    if(!holdUpdate) Vis.update();
}

var updateFocusable = function(){

    if(Vis.selected.length==0) Vis.focused = [];
    else {
        $.each(Vis.focused, function (i, v) {
            if ((Vis.selected.indexOf(v)) == -1) Vis.focused.splice(i, 1);
        });
    }
    $('#country-focus-text').text( ((Vis.focused.length==1)? "1 country":  Vis.focused.length + " countries") + " selected");

    $.each($(Vis.countryFocusSelector + " > div"), function(index, element) {
        var showElement = Vis.selected.indexOf($(element).attr('country')) > -1;

        $(element).find( 'input').prop('checked', Vis.focused.indexOf($(element).attr('country')) > -1);

        if (showElement){
            $(element).show();
        }
        else{
            $(element).hide();
        }
    });
};

var update_selected = function(){
    $.each($(".continent-selector > div"), function(index, element) {
        $(element).find( 'input').prop('checked', Vis.selected.indexOf($(element).attr('country')) > -1);
    });
};

var set_country_selector_overlay_height = function(){

    var outerContainer = $('#selector-overlay');
    var resultHeight = outerContainer.height();

    resultHeight -= outerContainer.children('.toolbar').outerHeight();
    $.each(outerContainer.children('.wrapper').children('form'), function(i,v){
        resultHeight -= $(v).outerHeight();
    });

    var panel_body = $(".panel-body.calc-height");
    $.each(panel_body, function(i, v) {
        panel_body.outerHeight(0.9 * resultHeight - $(v).prev().outerHeight());
    });

};

var country_selector_filter_initialize = function(){

    var $filter = $('#selector-filter');

    var $conts = $('.continent-selector');
    var $originalCountries = {};
    $.each($conts, function(i,v){
        $originalCountries[i] = $(v).children('div');
    });

    var _filter = function(val)
    {
        $.each($conts, function(i,v){
            v = $(v);
            var $filterElems = $originalCountries[i].filter(function(i, el){
                return $(el).attr('country').toLowerCase().indexOf(val.toLowerCase()) > -1;
            });
            v.children('div').detach();
            $filterElems.appendTo(v);
        });
    };

    $filter.on('change paste keyup',function(event){
        _filter($(this).val());
    });

    $filter.siblings("span").children('span.glyphicon-remove-circle').click(function(){
        $filter.val("").trigger('change');
        $filter.focus();
    })


};


var initialize_continents = function(){

    var checkStatus = {};

    $.each(continents, function(i,v){

        checkStatus[v] = {};
        checkStatus[v].sort = 1;
        checkStatus[v].check = 0;

        $("#"+ v +"-check-all").click(function(){
            var $elems = $("#" + v + "-country-list > div");
            $.each($elems, function(i2,v2){
                var idx;
                if (( idx = Vis.selected.indexOf( $(v2).attr('country') ) ) > -1 ) {
                    if(!checkStatus[v].check) Vis.selected.splice( idx, 1 );
                } else {
                    if(checkStatus[v].check) Vis.selected.push( $(v2).attr('country') );
                }
            });
            checkStatus[v].check = 1 - checkStatus[v].check;
            update_selected();
        });
    });
};

var initialize_buttons = function(){

    // General overlay
    $('#overlay-shade, .overlay a').on('click', function(e) {
        closeOverlay();
        Vis.update();
        if ($(this).attr('href') == '#') e.preventDefault();
    });


    // Reset button
    $("#reset").click(function(){
        Vis.reset();
        initialize_subroutine();
        update_selected();
        updateFocusable();
        Vis.update();
    });

    // Credits button
    $('#credits').click(function(e) {
        openOverlay('#credits-overlay');
        e.preventDefault();
    });

    // Enable countries overlay
    $('#country-select').click(function(e) {
        openOverlay('#selector-overlay');
        e.preventDefault();
    });

    /* Enable countries section */
    $('#country-select-all').click(function(e){Vis.selected = Vis.selectableCountries.slice(); Vis.update();});
    $('#country-disable-all').click(function(e){Vis.selected = []; Vis.update();});
    $('#country-select-focused').click(function(e){Vis.selected = Vis.focused.slice(); Vis.update();});


    // Initialize the country selector button:
    Vis.data.forEach(function(d, i){
        if(Vis.selected.indexOf(d.country)>-1) {
            var option = $("<div>")
                .attr('country', d.country)
                .attr('value', +d[Vis.selectedYear][Vis.defaultSeries])
                .addClass("clickable")
                .addClass("row")
                .addClass("dropdown");
            option.append(
                $("<input>").attr('type', 'checkbox')
                    .addClass("clickable")
                    .addClass("col-xs-1")
            );
            option.append(
                $("<span>").html(" " + d.country)
                    .addClass("pull-left")
                    .addClass("col-xs-9")
            );
            option.append(
                $("<span>")
                    .addClass("pull-right")
                    .addClass("col-xs-2")
                    .addClass(i + "-selector-val")
                    .addClass("internet-value")
                    .html((+d[Vis.selectedYear][Vis.defaultSeries]).toFixed(2))
            );
            $(Vis.countryFocusSelector).append(option);
        }
    });

    // Set the filter to visible on the click event
    $('#country-focus').on('click', function(event){
        $('.filter').show();
    });

    // Set the click event for items in the menu
    $(Vis.countryFocusSelector + " > div" )
        .on( 'click', function( event ) {

        var $target = $( event.currentTarget ),
            val = $target.attr( 'country' ),
            $inp = $target.find( 'input' );

        toggleClick(val, $inp);
        $( event.target ).blur();

        if(event.target.type != "checkbox") {
            event.preventDefault();
            return false;
        }
    });

    var $allElems = $('#country-focus-list > div');
    var $filterElems = $allElems.slice();

    var _sort = function(element, sortOn, ascending) {

        // select the children of the element
        var $elems = $(element + " > * ");

        // sort them according to the parameters
        $elems.sort(function (a, b) {

            var valueA = 0, valueB = 0;

            if (sortOn == "checked"){
                valueA = ($(a).find('input').prop('checked'))?0:1;
                valueB = ($(b).find('input').prop('checked'))?0:1;
            }
            if (sortOn == "internet") {
                valueB = +$(a).attr('value');
                valueA = +$(b).attr('value');
            }
            if (valueA == valueB) {
                valueA = $(a).attr('country');
                valueB = $(b).attr('country');
            }

            if (valueA > valueB) {
                return -ascending;
            }
            if (valueA < valueB) {
                return ascending;
            }
            return 0;
        });

        // rebuild the DOM
        $elems.detach().appendTo($(element));
    };

    var _filter = function()
    {
        // select the children of the element
        var $elems = $allElems.slice();
        var element = '#country-focus-list';

        $elems = subfilter(element, 'country', $('#focus-search-country').val(), $elems);
        subfilter(element, 'internet', $('#focus-search-internet').val(), $elems);

    };
    var subfilter = function(element, filterOn, value, $elems){


        if(filterOn == "internet") {
            var values = value.split("&&");

            var filterFunctions = [];
            $.each(values, function(i, value){
                var _resF;
                value = value.trim();

                if(value[value.length-1] == ".") value = value.substr(0,value.length-1) + "0";

                if(value.substring(0,2) == ">=") _resF = function(v){ return v >= (+value.substring(2));};
                else if(value.substring(0,2) == "<=") _resF = function(v){ return v <= (+value.substring(2));};
                else if(value[0] == ">") _resF = function(v){ return v > (+value.substring(1));};
                else if(value[0] == "<") _resF = function(v){ return v < (+value.substring(1));};
                else _resF = function(v){ return (""+v).indexOf(""+value) == 0;};

                filterFunctions[i] = _resF;
            });
        }
        // filter elements that do not match the parameters
        $filterElems = $elems.filter(function (i, el) {
            if(filterOn == "country"){
                return $(el).attr('country').toLowerCase().indexOf(value.toLowerCase()) > -1;
            }
            else{
                var result = true;
                $.each(filterFunctions, function(i, f){
                   result = result && (f(+$(el).attr('value')));
                });
                return result;
            }
        });

        // rebuild the DOM
        $elems.detach();
        $filterElems.appendTo($(element));
        return $filterElems;

    };

    var sortStatus = {
        'selectAll' : -1,
        'checked' : -1,
        'country' : -1,
        'internet' : -1
    };

    // set the select all button within the filter
    $('#focus-all-filtered').click(function(e){
        var caret = $(this).find('span.is-selected-icon');

        caret.toggleClass('glyphicon-unchecked');
        caret.toggleClass('glyphicon-collapse-down');

        sortStatus.selectAll*=-1;

        $.each($filterElems, function(i, val) {
            toggleClick($(val).attr('country'), $(val).find('input'), sortStatus.selectAll, true);
        });

        Vis.update();

        e.preventDefault();
        return false;
    });


    // Set the sort buttons
    $('.sort-button').click(function(e){

        var caret = $(this).find('span.sort-direction');
        var ascending = (sortStatus[$(this).attr('sortOn')] *= -1);

        caret.toggleClass('glyphicon-menu-down');
        caret.toggleClass('glyphicon-menu-up');

        _sort('#country-focus-list', $(this).attr('sortOn'), ascending);
        e.preventDefault();
        return false;
    });

    // set the filter options
    $('#focus-search-country')
        .bind('keypress', function (event) {
            var regex = new RegExp("^[^0-9]+$");
            var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
            if (!regex.test(key)) {
                event.preventDefault();
                return false;
            }
        })
        .on('change paste keyup',function(event){
            _filter();
        });

    $('#focus-search-internet')
        .bind('keypress', function (event) {
            var regex = new RegExp("^\d{0,3}\.?\d{0,2}$");
            var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
            if (!regex.test(key)) {
                event.preventDefault();
                return false;
            }
        })
        .on('change paste keyup',function(event){
            _filter();
        });


    /* Selected Countries section */
    $('#country-focus-all').click(function(e){Vis.focused= Vis.selected.slice(); Vis.update();  });
    $('#country-unfocus-all').click(function(e){Vis.focused= []; Vis.update();});

    var $select = $("#country-focused-selected");
    var g7 = ["Canada", "France", "Germany", "Italy", "Japan", "United Kingdom", "United States"];
    var eu = ["Austria", "Belgium", "Bulgaria",  "Croatia",  "Cyprus",  "Czech Rep.",  "Denmark",  "Estonia",
        "Finland",  "France",  "Germany",  "Greece",  "Hungary",  "Ireland",  "Italy",  "Latvia",  "Lithuania",
        "Luxembourg",  "Malta",  "Netherlands",  "Poland",  "Portugal",  "Romania",  "Slovakia",  "Slovenia",
        "Spain",  "Sweden",  "United Kingdom"];


    $.each($select.next().children('li'), function(i,v){
        $(v).click(function(s){
            switch ($(v).attr('item')){
                case "top5": Vis.focused = [];$.each(Vis.data.filter(function(v){return Vis.selected.indexOf(v.country)>-1}).sort(function(a,b){return (+b[Vis.selectedYear][Vis.defaultSeries]) - (+a[Vis.selectedYear][Vis.defaultSeries]);}).slice(0,5), function(i,v){Vis.focused.push(v.country)}); break;
                case "top10": Vis.focused = [];$.each(Vis.data.filter(function(v){return Vis.selected.indexOf(v.country)>-1}).sort(function(a,b){return (+b[Vis.selectedYear][Vis.defaultSeries]) - (+a[Vis.selectedYear][Vis.defaultSeries]);}).slice(0,10), function(i,v){Vis.focused.push(v.country)}); break;
                case "bottom5": Vis.focused = [];$.each(Vis.data.filter(function(v){return Vis.selected.indexOf(v.country)>-1}).sort(function(a,b){return (+a[Vis.selectedYear][Vis.defaultSeries]) - (+b[Vis.selectedYear][Vis.defaultSeries]);}).slice(0,5), function(i,v){Vis.focused.push(v.country)}); break;
                case "bottom10": Vis.focused = [];$.each(Vis.data.filter(function(v){return Vis.selected.indexOf(v.country)>-1}).sort(function(a,b){return (+a[Vis.selectedYear][Vis.defaultSeries]) - (+b[Vis.selectedYear][Vis.defaultSeries]);}).slice(0,10), function(i,v){Vis.focused.push(v.country)}); break;
                case "g7": Vis.focused = (g7.slice()).filter(function(v){return Vis.selected.indexOf(v)>-1}); break;
                case "eu":  Vis.focused = (eu.slice()).filter(function(v){return Vis.selected.indexOf(v)>-1}); break;
            }
            Vis.update();
        });
    });

};

var initialize_country_selector = function() {

    var selector_top_number= $('#selector-top-number');
    var selector_top_year = $('#selector-top-year');
    var selector_bottom_number = $('#selector-bottom-number');
    var selector_bottom_year = $('#selector-bottom-year');

    var buttons = [selector_top_number, selector_top_year, selector_bottom_number, selector_bottom_year];

    var intervals = {};

    buttons.forEach(function(v,i){
        intervals[i] = {};
        (v.prev())
        .mousedown(function(e){
            v.val(Math.max(v.attr('min'), +v.val()-1));

            intervals[i].timeout = setTimeout(function() {
                intervals[i].interval = setInterval(function(){v.val(Math.max(v.attr('min'), +v.val()-1))}, 100);
            }, 300);
        })
        .mouseup(function(){
                clearTimeout(intervals[i].timeout);
                clearInterval(intervals[i].interval)
            });

        (v.next())
        .mousedown(function(e){
            v.val(Math.min(v.attr('max'), +v.val()+1));

            intervals[i].timeout2 = setTimeout(function() {
                intervals[i].interval2 = setInterval(function () {
                    v.val(Math.min(v.attr('max'), +v.val() + 1))
                }, 100);
            }, 300);
        })
        .mouseup(function(){
                clearTimeout(intervals[i].timeout2);
                clearInterval(intervals[i].interval2);
            });
    });

    $('#selector-top-select').click(function(){
        var sorted = Vis.data.sort(function(a,b){
           return +(b[+selector_top_year.val()][Vis.defaultSeries]) - +(a[+selector_top_year.val()][Vis.defaultSeries]);
        });
        var filtered = sorted.slice(0, +selector_top_number.val());
        Vis.selected = [];
        $.each(filtered, function(_,c){
            Vis.selected.push(c.country);
        });
        update_selected();
    });
    $('#selector-bottom-select').click(function(){
        var sorted = Vis.data.sort(function(a,b){
            return +(a[+selector_bottom_year.val()][Vis.defaultSeries]) - (b[+selector_bottom_year.val()][Vis.defaultSeries]);
        });
        var filtered = sorted.slice(0, +selector_bottom_number.val());
        Vis.selected = [];
        $.each(filtered, function(_,c){
            Vis.selected.push(c.country);
        });
        update_selected();
    });






    var continent_elements = {};

    $.each(continents, function(i, v){
        continent_elements[i] = {};

        var continent = $("<div>")
            .addClass("col-xs-2")
            .append($("<div>")
                .addClass("panel panel-default")
                .append($("<div>")
                    .addClass("panel-heading col-xs-12 no-padding")
                    .append($("<div>")
                        .attr('id', v+"-name")
                        .addClass("col-xs-8")
                        .append($("<span>")
                            .addClass("panel-title text-center")
                            .text(" " +i)
                        )
                    ).append($("<div>")
                        .attr('id', v+"-check-all")
                        .addClass('col-xs-2')
                        .append($("<span>")
                            .addClass("btn glyphicon glyphicon-unchecked")
                        )
                    )
                )
                .append($("<div>")
                    .addClass("panel-body in-overlay calc-height col-xs-12")
                    .append($("<div>")
                        .addClass("col-xs-12 continent-selector")
                        .attr('id', v + "-country-list")
                    )
                )
            );

        var continent_selector_container = $('#continent-selector-container');
        continent_selector_container.append(continent);

        continent_elements[i].name = $("#"+v+"-name");
        continent_elements[i].checkAll = $("#"+v+"-check-all");
        continent_elements[i].list = $("#"+v + "-country-list");
    });

    var clickCountry = function(){
        var c = $(this).attr('country');
        var idx;
        if((idx = Vis.selected.indexOf(c)) == -1){
            Vis.selected.push(c);
        }
        else{
            Vis.selected.splice(idx, 1);
        }
        update_selected();
    };

    $.each(Vis.data, function(i, v){
      if(v.continent) {

          var option = $("<div>")
              .attr('country', v.country)
              .addClass("clickable")
              .append($('<span>')
                  .text(v.country)
              )
              .append($('<input>')
                  .attr('type', 'checkbox')
                  .addClass('pull-right')
              )
              .click(clickCountry);

          continent_elements[v.continent].list.append(option);
      }
    });

    country_selector_filter_initialize();
    initialize_continents();

};

var initialize_animation_buttons = function(){
    var v = $('#animation-speed');
    var intervals = [];
    (v.prev())
        .mousedown(function(e){
            v.val(Math.max(v.attr('min'), +v.val()-1));
            v.change();

            intervals[0] = setTimeout(function() {
                intervals[1] = setInterval(function(){v.val(Math.max(v.attr('min'), +v.val()-1))}, 100);
                v.change();
            }, 300);
        })
        .mouseup(function(){
            clearTimeout(intervals[0] );
            clearInterval(intervals[1] )
        });

    (v.next())
        .mousedown(function(e){
            v.val(Math.min(v.attr('max'), +v.val()+1));
            v.change();

            intervals[2] = setTimeout(function() {
                intervals[3]  = setInterval(function () {
                    v.val(Math.min(v.attr('max'), +v.val() + 1));
                    v.change();
                }, 100);
            }, 300);
        })
        .mouseup(function(){
            clearTimeout(intervals[2]);
            clearInterval(intervals[3]);
        });


    var play = $("#start-animation");
    var stop = $("#stop-animation");
    var playing = false;
    var animation;

    play.click(function(){
        playing = true;
        play.addClass("active");
        clearInterval(animation);
        animation = setInterval(function () {
            var diff = Vis.yearRange[1] - Vis.yearRange[0];
            var newYear = (((Vis.selectedYear + 1) - Vis.yearRange[0]) % (diff + 1)) + Vis.yearRange[0];
            Vis.selectYear(newYear);
        }, 4000 - $('#animation-speed').val() * 300);
    });
    stop.click(function(){
        playing = false;
        play.removeClass("active");
        clearInterval(animation);
    });

    v.change(function(){
        if(playing){
            clearInterval(animation);
            play.trigger('click');
        }
    })
};

var initialize_subroutine = function(){

    initialize_yearSlider(Vis.yearRangeSlider);
    initialize_yearSelector('#year-selector');
    initialize_animation_buttons();
    initialize();
    visualize();
};

function openOverlay(olEl) {
    var $oLay = $(olEl);

    if ($('#overlay-shade').length == 0)
        $('body').prepend('<div id="overlay-shade"></div>');

    $('#overlay-shade').fadeTo(300, 0.6, function() {
        var props = {
            oLayWidth       : $oLay.width(),
            scrTop          : $(window).scrollTop() + 1.5 * $(window).height()/10,
            viewPortWidth   : $(window).width()
        };

        var leftPos = (props.viewPortWidth - props.oLayWidth) / 2;

        $oLay
            .css({
                display : 'block',
                opacity : 0,
                top : '-=300',
                left : leftPos+'px'
            })
            .animate({
                top : props.scrTop,
                opacity : 1
            }, 600);

        set_country_selector_overlay_height();
        update_selected();
    });
}

function closeOverlay() {
    $('.overlay').animate({
        top : '-=300',
        opacity : 0
    }, 400, function() {
        $('#overlay-shade').fadeOut(300);
        $(this).css('display','none');
    });
}

var initialize_yearSlider = function (name){

    $(name).slider({
        range:true,
        min: +Vis.startYear,
        max: +Vis.endYear,
        values: [+Vis.startYear, +Vis.endYear]
    });

    // Add lines / pips to range slider
    $(name).slider('pips', {
        rest : "label",
        prefix: "",
        suffix: ""
    });

    // Add floating elements to slider
    $(name).slider('float', {
        handle: true,
        pips: true
    });

};

var initialize_yearSelector = function (name){

    var yearRange = $(Vis.yearRangeSlider).slider("option", "values");
    Vis.yearRange = yearRange;

    Vis.yearSelector = $(name).slider({
        min: +yearRange[0],
        max: +yearRange[1],
        value: +Vis.selectedYear,
        slide: function( event, ui ) {
            if(ui.value > yearRange[1] || ui.value < yearRange[0]) return false;
            Vis.selectedYear = Math.max(yearRange[0], Math.min(yearRange[1],ui.value));
            Vis.yearSelector.slider("value", Vis.selectedYear);
            Vis.selectYear(Vis.selectedYear);
        },
        change: function( event, ui){
            if(ui.value == Vis.selectedYear) return false;
            Vis.selectedYear = Math.max(yearRange[0], Math.min(yearRange[1],ui.value));
            Vis.yearSelector.slider("value", Vis.selectedYear);
            Vis.selectYear(Vis.selectedYear);
        }

    });

    // Add lines / pips to range slider
    $(name).slider('pips', {
        rest : "label",
        prefix: "",
        suffix: ""
    });

    // Add floating elements to slider
    $(name).slider('float', {
        handle: true,
        pips: true
    });

    $(Vis.yearRangeSlider).on("slide", function(event, ui){
        Vis.selectedYear = Math.max(ui.values[0], Math.min(ui.values[1],Vis.yearSelector.slider("value")));
        Vis.yearSelector.slider("value", Vis.selectedYear);
        Vis.yearRange = ui.values;
        Vis.selectYear(Vis.selectedYear);
    });

};


var initialize = function () {

    Vis.setView(Vis.validViews[0], Vis.typeEnum["Bar Chart"]);
    Vis.setView(Vis.validViews[1], Vis.typeEnum["Map"]);
    Vis.setView(Vis.validViews[2], Vis.typeEnum["Pie Chart"]);
    Vis.setView(Vis.validViews[3], Vis.typeEnum["Bubble Chart"]);

};

var visualize = function () {
    for (var v in Vis.views) {
        if (Vis.views.hasOwnProperty(v))
            Vis.views[v].visualize();
    }
};

