/**
 * Created by sjhoo on 21-10-2015.
 */

var DottedChart = function() {

    this.name = "Bubble Chart";
    Visualization.apply(this, arguments);
    this.xSeries = this.serie;
    this.ySeries = this.serie;
    this.radiusSeries = this.serie;
    this.xMin = this.vis.series[this.xSeries].min;
    this.yMin = this.vis.series[this.ySeries].min;
    this.rMin = this.vis.series[this.radiusSeries].min;
    this.resize();
};

DottedChart.prototype = Object.create(Visualization.prototype);
DottedChart.prototype.constructor = DottedChart;

DottedChart.prototype.initializeVisualization = function(){
    Visualization.prototype.initializeVisualization.call(this);

    $(this.container +'-series').remove();

    $(this.container+"-svg").before($('<div>')
        .attr('class', 'clearfix row')
            .append($('<div>')
                .attr('id', this.container.slice(1) + "-radius-button")
                .attr("class", "col-xs-2 btn btn-small btn-primary margin-top-md pull-right margin-right-md dropdown-toggle "  + this.container.slice(1)+"-series-toggler")
                .attr('type', 'button')
                .attr('elem', 'r')
                .attr('data-toggle','dropdown')
                .html("Radius data"))
            .append($('<div>')
                .attr('id', this.container.slice(1) + "-x-button")
                .attr("class", "col-xs-2 btn btn-small btn-primary pull-right margin-top-md margin-right-md dropdown-toggle " + this.container.slice(1)+"-series-toggler")
                .attr('type', 'button')
                .attr('data-toggle','dropdown')
                .attr('elem', 'x')
                .html("X axis"))
            .append($('<div>')
                .attr('id', this.container.slice(1) + "-y-button")
                .attr("class", "col-xs-2 btn btn-small btn-primary pull-right margin-top-md margin-right-md dropdown-toggle " + this.container.slice(1)+"-series-toggler")
                .attr('type', 'button')
                .attr('data-toggle','dropdown')
                .attr('elem', 'y')
                .html("Y axis"))
            .append($('<div>')
                .attr("class", "col-xs-2 col-xs-offset-1 pull-left margin-top-md")
                .append($("<span>")
                    .attr('id', this.container.slice(1) + "-year")
                    .attr("class","h1")
                    .text(this.year) ))
    );

};

DottedChart.prototype.selectYear = function(year){
    Visualization.prototype.selectYear.call(this, year);
    $(this.container+"-year").text(this.year);
};

DottedChart.prototype.resize = function(){
    Visualization.prototype.resize.call(this);

    this.innerGraphic.attr('width', this.width)
        .attr('height', this.height);

    this.topPadding = this.height * 0.1;
    this.bottomPadding = this.height * 0.15;
    this.leftPadding = this.width * 0.1;
    this.rightPadding = this.width  * 0.05;

    if(this.path) this.update();
};

DottedChart.prototype.selectYear = function(year){
    Visualization.prototype.selectYear.call(this, year);
    this.update();
    $(this.container+"-year").text(this.year);
};

DottedChart.prototype.update = function(){

    var dottedchart = this;
    var getValue = function(d, type){
        var val;
        var min;
        switch(type) {
            case "x" :
                val = d[dottedchart.year][dottedchart.xSeries];
                min = dottedchart.xMin;
                break;
            case "y" :
                val = d[dottedchart.year][dottedchart.ySeries];
                min = dottedchart.yMin;
                break;
            case "r" :
                val = d[dottedchart.year][dottedchart.radiusSeries];
                min = dottedchart.rMin;
                break;
        }

        if ($.isNumeric(val) && val === val) return val;
        return min;
    };



    this.updateScales();


    this.path
        .transition()
        .duration(400)
        .attr("transform", function(d) { return "translate(" + dottedchart.xScale(getValue(d,"x")) + "," + dottedchart.yScale(getValue(d,"y")) + ")"; })
        .attr("r", function(d) { if (dottedchart.vis.selected.indexOf(d.country)== -1) return 0; else return dottedchart.radius(getValue(d,"r"))})
        .style('stroke', function(d,i){
            if(dottedchart.vis.focused.indexOf(d.country)>-1) return dottedchart.vis.selectorEnum.COUNTRY;
            return d3.rgb(dottedchart.color(d.country)).darker(1);
        })
        .style('stroke-width', function(d){
            return (dottedchart.vis.focused.indexOf(d.country)>-1)? 3 : 1;
        });

};

DottedChart.prototype.getMin = function(serie){
    var dottedchart = this;
    var local = this.data.slice(0).filter(function(el){return dottedchart.vis.selected.indexOf(el.country) > -1});
    var min = +local[0][dottedchart.year][serie];
    $.each(local, function(i, v){
        if(+v[dottedchart.year][serie] < min) min = +v[dottedchart.year][serie]
    });
    if(min !== min) return this.vis.series[serie].min;
    return min;

};

DottedChart.prototype.getMax = function(serie){
    var dottedchart = this;
    var local = this.data.slice(0).filter(function(el){return dottedchart.vis.selected.indexOf(el.country) > -1});
    var max = +local[0][dottedchart.year][serie];
    $.each(local, function(i, v){
        if(+v[dottedchart.year][serie] > max) max = +v[dottedchart.year][serie]
    });
    if(max !== max) return this.vis.series[serie].max;
    return max;
};

DottedChart.prototype.updateScales = function(){

    this.xMin = this.getMin(this.xSeries);
    this.yMin = this.getMin(this.ySeries);
    this.rMin = this.getMin(this.radiusSeries);

    this.xScale = d3.scale.linear()
        .range([this.leftPadding, this.width - this.rightPadding])
        .domain([this.xMin,this.getMax(this.xSeries)]);
    this.yScale = d3.scale.linear()
        .range([this.height-this.topPadding, this.bottomPadding])
        .domain([this.yMin,this.getMax(this.ySeries)]);
    this.radius = d3.scale.linear()
        .range([0,20])
        .domain([this.rMin,this.getMax(this.radiusSeries)]);

    this.xAxis = d3.svg.axis()
        .scale(this.xScale)
        .orient("bottom")
        .tickFormat(this.getReadableValue);

    this.yAxis = d3.svg.axis()
        .scale(this.yScale)
        .orient("left")
        .tickFormat(this.getReadableValue);

    this.xAxisElem
        .transition()
        .duration(400)
        .attr("transform", "translate(0," + (this.height-this.topPadding) + ")")
        .call(this.xAxis);

    this.yAxisElem
        .transition()
        .duration(400)
        .attr("transform", "translate(" + this.leftPadding + ",0)")
        .call(this.yAxis);

};

DottedChart.prototype.visualize = function(){
    var data = this.data;
    var vis = this.vis;
    var color = this.color;
    var dottedchart = this;


    var series_elem = d3.select(this.container).append("ul")
        .attr("id", this.container.slice(1) + "-series")
        .style("position", "absolute")
        .style("z-index", "10")
        .style("display", "")
        .attr("class", "dropdown-menu padding-top-3")
        .attr('aria-labelledby', this.container+"=radius-button")
        .attr('role', 'menu')
        .on('mouseleave', function(){
            d3.select(this).style('display','none');
        });

    series_elem.selectAll()
        .data(Object.keys(vis.series))
        .enter()
        .append('li')
            .append('a')
            .attr('href', '#')
            .attr('series', function(key){return key;})
            .text(function(key){ return vis.series[key].name});


    d3.selectAll("." + this.container.slice(1)+"-series-toggler")
        .on("click", function(d){
            var position = $("." + dottedchart.container.slice(1)+"-series-toggler").filter(":first").position();

            var change = $(this).attr('elem');

           series_elem
               .style("display", "block")
               .style("left",  (position.left - $(dottedchart.container+"-series").width()) + "px")
               //.style("left",  ($(this).width() + position.left - series_elem.node().getBoundingClientRect().width) + "px")
               .style("top",  (position.top + $(this).height() )+ "px")
               .selectAll('li')
               .on("click", function(d){
                    switch(change){
                        case "x" : dottedchart.xSeries = d; break;
                        case "y" : dottedchart.ySeries = d; break;
                        case "r" : dottedchart.radiusSeries = d; break;
                    }
                   series_elem.style("display", "none");
                   dottedchart.update();
               });
        });


    this.xAxisElem = this.svg
        .append("g")
        .attr("class", "x axis");

    this.yAxisElem = this.svg
        .append("g")
        .attr("class", "y axis");

    this.updateScales();

    // Add tooltip for dottedchart elements
    var tooltip = d3.select("body").append("div")
        .attr("id", this.container + "-tooltip-dottedchart")
        .attr("class", "tooltip")
        .style("opacity", 0)
        .style("position", "absolute")
        .style("z-index", "10")
        .style("opacity", 0);

    this.path = this.svg
        .append('g')
        .attr("class", "dottedchart")
        .selectAll("circle")
        .data(data)
        .enter()
        .append("circle")
        .attr("class", function(d){
            return ("country " + ((vis.selected.indexOf(d.country)>-1) ? "clickable" : ""));
        })
        .attr('fill', function(d, i) {
            return dottedchart.color(d.country);
        })
        .style('stroke', function(d,i){
            if(vis.focused.indexOf(d.country)>-1) return vis.selectorEnum.COUNTRY;
            return d3.rgb(color(d.country)).darker(1);
        })
        .style('stroke-width', function(d){
            return (vis.focused.indexOf(d.country)>-1)? 3 : 1;
        })
        .on("mouseover.tooltip", function(d){
            tooltip.html(
                d.country + "<br/> x : "  + dottedchart.getReadableValue(+d[dottedchart.year][dottedchart.xSeries]) +
                "<br/> y : "  + dottedchart.getReadableValue(+d[dottedchart.year][dottedchart.ySeries]) +
                 "<br/> r : "  + dottedchart.getReadableValue(+d[dottedchart.year][dottedchart.radiusSeries]))
                .transition()
                .duration(200)
                .style("opacity", .95);
        })
        .on("mousemove.tooltip", function(d){
            tooltip
                .style("left", d3.event.pageX + 10 + "px")
                .style("top", (d3.event.pageY - 10) + "px");

        })
        .on("mouseout.tooltip", function(d) {
            tooltip.transition()
                .duration(500)
                .style("opacity", 0);
        })
        .on("click", function(d) {
            if(vis.selected.indexOf(d.country)>-1) {
                dottedchart.toggleFocusCountry(d.country);
                dottedchart.update();
            }
        })
        .on("mouseover", function (d) {
            if(vis.selected.indexOf(d.country)>-1) {
                d3.select(this.parentNode.appendChild(this))
                    .style('stroke-width', 3)
                    .transition()
                    .duration(400);
            }
        })
        .on("mouseout", function (d) {
            d3.select(this)
                .style('stroke-width', function(b){
                    return (vis.focused.indexOf(d.country)>-1)?3:1;
                })
                .transition()
                .duration(400)
        });

    this.update();


};
