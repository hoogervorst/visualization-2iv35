/**
 @constructor
 @abstract
 */
var Visualization = function(main, vis) {
    if (this.constructor === Visualization) {
        this.name = "Select Type";
     //   throw new Error("Can't instantiate abstract class!");
    }

    if(!Vis) throw  new Error("This class requires a global object named Vis that contains the data linking the different visualizations");
    this.container = main;
    this.year = Vis.selectedYear;
    this.serie = vis.defaultSeries;
    this.initializeVisualization();
    this.vis = vis;
    this.data = vis.data;
    this.color = d3.scale.ordinal()
        .range([ "#000000", "#FFFF00", "#1CE6FF", "#FF34FF", "#FF4A46", "#008941", "#006FA6", "#A30059",
            "#FFDBE5", "#7A4900", "#0000A6", "#63FFAC", "#B79762", "#004D43", "#8FB0FF", "#997D87",
            "#5A0007", "#809693", "#FEFFE6", "#1B4400", "#4FC601", "#3B5DFF", "#4A3B53", "#FF2F80",
            "#61615A", "#BA0900", "#6B7900", "#00C2A0", "#FFAA92", "#FF90C9", "#B903AA", "#D16100",
            "#DDEFFF", "#000035", "#7B4F4B", "#A1C299", "#300018", "#0AA6D8", "#013349", "#00846F",
            "#372101", "#FFB500", "#C2FFED", "#A079BF", "#CC0744", "#C0B9B2", "#C2FF99", "#001E09",
            "#00489C", "#6F0062", "#0CBD66", "#EEC3FF", "#456D75", "#B77B68", "#7A87A1", "#788D66",
            "#885578", "#FAD09F", "#FF8A9A", "#D157A0", "#BEC459", "#456648", "#0086ED", "#886F4C",

            "#34362D", "#B4A8BD", "#00A6AA", "#452C2C", "#636375", "#A3C8C9", "#FF913F", "#938A81",
            "#575329", "#00FECF", "#B05B6F", "#8CD0FF", "#3B9700", "#04F757", "#C8A1A1", "#1E6E00",
            "#7900D7", "#A77500", "#6367A9", "#A05837", "#6B002C", "#772600", "#D790FF", "#9B9700",
            "#549E79", "#FFF69F", "#201625", "#72418F", "#BC23FF", "#99ADC0", "#3A2465", "#922329",
            "#5B4534", "#FDE8DC", "#404E55", "#0089A3", "#CB7E98", "#A4E804", "#324E72", "#6A3A4C",
            "#83AB58", "#001C1E", "#D1F7CE", "#004B28", "#C8D0F6", "#A3A489", "#806C66", "#222800",
            "#BF5650", "#E83000", "#66796D", "#DA007C", "#FF1A59", "#8ADBB4", "#1E0200", "#5B4E51",
            "#C895C5", "#320033", "#FF6832", "#66E1D3", "#CFCDAC", "#D0AC94", "#7ED379", "#012C58",

            "#7A7BFF", "#D68E01", "#353339", "#78AFA1", "#FEB2C6", "#75797C", "#837393", "#943A4D",
            "#B5F4FF", "#D2DCD5", "#9556BD", "#6A714A", "#001325", "#02525F", "#0AA3F7", "#E98176",
            "#DBD5DD", "#5EBCD1", "#3D4F44", "#7E6405", "#02684E", "#962B75", "#8D8546", "#9695C5",
            "#E773CE", "#D86A78", "#3E89BE", "#CA834E", "#518A87", "#5B113C", "#55813B", "#E704C4",
            "#00005F", "#A97399", "#4B8160", "#59738A", "#FF5DA7", "#F7C9BF", "#643127", "#513A01",
            "#6B94AA", "#51A058", "#A45B02", "#1D1702", "#E20027", "#E7AB63", "#4C6001", "#9C6966",
            "#64547B", "#97979E", "#006A66", "#391406", "#F4D749", "#0045D2", "#006C31", "#DDB6D0",
            "#7C6571", "#9FB2A4", "#00D891", "#15A08A", "#BC65E9", "#FFFFFE", "#C6DC99", "#203B3C",

            "#671190", "#6B3A64", "#F5E1FF", "#FFA0F2", "#CCAA35", "#374527", "#8BB400", "#797868",
            "#C6005A", "#3B000A", "#C86240", "#29607C", "#402334", "#7D5A44", "#CCB87C", "#B88183",
            "#AA5199", "#B5D6C3", "#A38469", "#9F94F0", "#A74571", "#B894A6", "#71BB8C", "#00B433",
            "#789EC9", "#6D80BA", "#953F00", "#5EFF03", "#E4FFFC", "#1BE177", "#BCB1E5", "#76912F",
            "#003109", "#0060CD", "#D20096", "#895563", "#29201D", "#5B3213", "#A76F42", "#89412E",
            "#1A3A2A", "#494B5A", "#A88C85", "#F4ABAA", "#A3F3AB", "#00C6C8", "#EA8B66", "#958A9F",
            "#BDC9D2", "#9FA064", "#BE4700", "#658188", "#83A485", "#453C23", "#47675D", "#3A3F00",
            "#061203", "#DFFB71", "#868E7E", "#98D058", "#6C8F7D", "#D7BFC2", "#3C3E6E", "#D83D66",

            "#2F5D9B", "#6C5E46", "#D25B88", "#5B656C", "#00B57F", "#545C46", "#866097", "#365D25",
            "#252F99", "#00CCFF", "#674E60", "#FC009C", "#92896B"])
        .domain(vis.selectableCountries);

};

/**
 @abstract
 */
Visualization.prototype.load = function() {
};

Visualization.prototype.selectYear = function(year){
    this.year = year;
};

Visualization.prototype.reset = function() {
    $(this.container).empty();
};

Visualization.prototype.visualize = function() {
   // throw new Error("Abstract method!");
};

Visualization.prototype.resize = function() {

    this.width = $(this.container).width();
    this.height = Math.max(0, $(this.container).height() - $(this.container+'-type').outerHeight(true) - $(this.container+'-year').outerHeight(true) - $(this.container+"-radius-button").outerHeight(true));

    this.svg.attr("width", this.width)
        .attr("height", this.height)
        .attr("style", "margin: 10px");

    this.innerGraphic
        .attr('transform', 'translate(' + (this.width / 2) +  ',' + (this.height / 2) + ')');
};

Visualization.prototype.toggleFocusCountry = function(country){
    var res = true;
    if(this.vis.focused.indexOf(country)>-1){
        this.vis.focused.splice(this.vis.focused.indexOf(country),1);
        res = false;
    }
    else {
        this.vis.focused.push(country);
    }
    this.vis.update();
    return res;
};

Visualization.prototype.initializeVisualization = function(){

    var type_items = "";

    for(var t in Vis.typeEnum)
    {
        if(Vis.typeEnum.hasOwnProperty(t)){
            type_items += '<li class="' + ((Vis.typeEnum[t] == null)?'disabled':'') +'"><a href="#">'+t+'</a></li>';
        }
    }

    var series_items = "";
    for(t in Vis.series){
        if(Vis.series.hasOwnProperty(t)){
            series_items += '<li><a href="#" series="'+ t + '">' + Vis.series[t].name +'</a></li>';
        }
    }

    $(this.container)
        .append(
        '<div class="clearfix">'+
        '       <div class="dropdown"> ' +
        '       <button id="'+ this.container.slice(1) +'-type" class="col-xs-3 btn btn-lg btn-default dropdown-toggle pull-right" type="button" data-toggle="dropdown">' + this.name +
        '       <span class="caret"></span></button> ' +
        '   <ul id='+ this.container.slice(1)+'-dropdown class="dropdown-menu dropdown-menu-right padding-top-3" role="menu" aria-labelledby="'+this.container.slice(1)+'-type"> ' +
        type_items +
        '   </ul></div> ' +
        '       <div class="dropdown"> ' +
        '       <button id="'+ this.container.slice(1) +'-series" class="col-xs-8 overflow-hidden btn btn-lg btn-default dropdown-toggle pull-left" type="button" data-toggle="dropdown">' + Vis.series[this.serie].name +
        '       <span class="caret"></span></button> ' +
        '   <ul id='+ this.container.slice(1)+'-dropdown-series class="dropdown-menu padding-top-3" role="menu" aria-labelledby="'+this.container.slice(1)+'-series"> ' +
        series_items +
        '   </ul></div> ' +
        '  </div>');


    $(this.container)
        .append('<div class="clearfix">'+
        '<div id='+this.container.slice(1)+'-svg class="vis-container fill">'+
        '</div></div>');


    var _this = this;

    $(this.container+"-dropdown > li > a").click(this.selectType());
    $(this.container+"-dropdown-series > li > a").click(function(){
       var elem = $(this);
       $(_this.container+"-series").html(Vis.series[elem.attr('series')].name + '  <span class="caret"></span>');
        _this.serie = elem.attr('series');
        _this.update();
    });


    this.svg = d3.select(this.container+"-svg")
        .append('svg');

    this.innerGraphic = this.svg.append('g');

};

Visualization.prototype.getReadableValue = function(value){
    if( value > 1000000000) return (Math.round(value/1000000000) + " B");
    else if( value > 1000000) return (Math.round(value/1000000) + " M");
    else if(value > 1000) return (Math.round(value/1000) + " K");
    else if (value < 100) return value.toFixed(2);
    else return Math.round(value);
};

Visualization.prototype.copyArray = function(a){
    return $.map(a, function (obj) {
        return $.extend(true, {}, obj);
    });
};

Visualization.prototype.update = function(){

};



Visualization.prototype.getSort = function(arg) {
    var vis = this.vis;
    var _this = this;

    if(!arg || arg == "value") {
        return function (a, b) {
            var bVal = +(b[vis.selectedYear][_this.serie]);
            var aVal = +(a[vis.selectedYear][_this.serie]);
            // Check if not NaN
            if(bVal === bVal)
            {
                if(aVal === aVal)
                    return bVal - aVal;
                return 1;
            }
            else {
                if(aVal === aVal)
                    return -1;
                else
                    return (a.country > b.country) ? 1 : -1;
            }
        };
    }
    else{
        return function(a,b){
            return (a.country > b.country)?1:-1;
        }
    }
};

Visualization.prototype.selectType = function(){
    var container = this.container;
    return function(event) {
        Vis.setView(container, Vis.typeEnum[event.target.innerHTML]);
        Vis.views[container].visualize();
    }
};