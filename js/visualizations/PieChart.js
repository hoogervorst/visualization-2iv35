/**
 * Created by sjhoo on 10-9-2015.
 */

var PieChart = function() {

    this.name = "Pie Chart";
    Visualization.apply(this, arguments);
    Visualization.prototype.resize.call(this);
    this.tier = {};
    this.tier.parentTier = this.tier;
    this.tier.current = this.vis.selectableCountries.slice();
    this.initializeData();
    this.updateData();
};

PieChart.prototype = Object.create(Visualization.prototype);
PieChart.prototype.constructor = PieChart;

PieChart.prototype.initializeVisualization = function() {

    Visualization.prototype.initializeVisualization.call(this);


    this.threshold = 0.05;
    this.minimumVisible = 4;

    $(this.container)
        .append('<div class="clearfix piechart bottom">'+
        ' <span id="'+this.container.slice(1) + '-year" class="h1 no-margin pull-left col-md-4 piechart">'+this.year + '</span>'+
        ' <div class="form-group form-group-sm col-md-4 text-center">' +
        '       <input type="number" class="form-control col-md-2 pull-right clickable" id="'+this.container.slice(1) + '-minimum" min="1" value="'+ (this.minimumVisible+1) +'" step="1" >' +
        '    <label for="'+this.container.slice(1) + '-minimum" class="clickable"> min. countries </label>' +
        ' <span class="help-block">Specify the minimum number of countries to display</span>' +
        '</div>'+
        ' <div class="form-group form-group-sm col-md-4 text-center">' +
        '       <input type="number" class="form-control col-md-2 pull-right clickable" id="'+this.container.slice(1) + '-threshold" min="0" max="100" value="'+ Math.round(this.threshold * 100) +'" step="1" >' +
        '    <label for="'+this.container.slice(1) + '-threshold" class="clickable"> % merge threshold </label>' +
        ' <span class="help-block">Specify the minimum percentage of area of the total the country should have </span>' +
        '</div>'+
        '</div>');

    var piechart = this;
    $(this.container+"-minimum").change(function(){
        piechart.minimumVisible = +$(this).val()-1;
        piechart.update();
    });
    $(this.container+"-threshold").change(function(){
        piechart.threshold = +$(this).val()/100;
        piechart.update();
    });
};

PieChart.prototype.setRadius = function(){
    this.radius = (Math.min(this.width, this.height) / 2) - 12;
};

PieChart.prototype.resize = function() {

  Visualization.prototype.resize.call(this);

    this.setRadius();
    this.setAnimation();
};

PieChart.prototype.getArc = function() {

    var radius = this.radius;
    return function(a) {
        var x;
        if(typeof(a) == "number") x = a;
        else x = a ? 1 : 0.95;
        return d3.svg.arc()
            .innerRadius(radius / 2 * x)
            .outerRadius(radius * x);
    }
};

PieChart.prototype.setAnimation = function() {
    var arc = this.getArc();
    var vis = this.vis;

    if (this.path) {

        this.path
            .each(function(d) {
                d3.select(this)
                    .transition()
                    .duration(400)
                    .attr("d", function (b) {
                        return arc(vis.focused.indexOf(d.data.country)>-1);
                    }())
            });

        this.path
            .on("mouseover", function (d) {
                d3.select(this.parentNode.appendChild(this))
                    .style('stroke-width', 3)
                    .transition()
                    .duration(400)
                    .attr("d", function(b){
                        return arc((vis.focused.indexOf(d.data.country)>-1)? true : 1);
                    }());
            })
            .on("mouseout", function (d) {
                d3.select(this)
                    .style('stroke-width', function(b){
                        return (vis.focused.indexOf(d.data.country)>-1)?3:1;
                    })
                    .transition()
                    .duration(400)
                    .attr("d", function (b) {
                        return arc(vis.focused.indexOf(d.data.country)>-1);
                    }());
            });
    }

    if (this.backButton) {
        this.backButton
            .transition()
            .duration(400)
            .attr("d", d3.svg.symbol().type('triangle-down').size(this.radius*this.radius / 9))
            .attr("transform", "translate(" + - (this.radius/6 * (1 - Math.tan(Math.PI / 6))) + ",0) rotate(90)");
    }
};

PieChart.prototype.toggleBackButton = function(arg){
    if(arg == "visible"){
        this.backButton
            .attr("visibility", "visible")
            .transition()
            .duration(400)
            .attr("opacity",1);
    }
    if(arg == "invisible"){
        this.backButton
            .transition()
            .duration(400)
            .attr("opacity", 0);
        this.backButton
            .transition()
            .delay(400)
            .attr("visibility", "hidden");
    }
};

PieChart.prototype.initializeData = function(year){
    Visualization.prototype.selectYear.call(this, year);

    this.data = this.copyArray(this.vis.data);
    this.other = {"country":'Other', 'value':0};
    for(var i = this.vis.startYear; i <= this.vis.endYear; i++)
    {
        this.other[i] = {};
    }
    this.data.push(this.other);
    this.data.sort(this.getSort(this.vis));
};

PieChart.prototype.selectYear = function(year){
     if(this.year == year) return;

     Visualization.prototype.selectYear.call(this, year);
     this.update();
    $(this.container+"-year").text(this.year);
};



PieChart.prototype.zoomIn = function() {
    this.tier = this.tier.childTier;
    if(this.tier!=this.tier.parentTier) this.toggleBackButton("visible");
    this.update();
};

PieChart.prototype.zoomOut = function () {
    this.tier = this.tier.parentTier;
    if (this.tier == this.tier.parentTier) this.toggleBackButton("invisible");
    this.update();
};

// updateData calculates what all the value attributes should be based on the internet values and whether
// the user has each country selected.
PieChart.prototype.updateData = function() {

    var localData = this.data.slice();

    // Sort a local copy of the array (with the same references) so
    // d3.pie does not see the data array as changed
    localData.sort(this.getSort());

    if(this.vis.selected.length < this.minimumVisible){
        while(this.tier != this.tier.parentTier) {
            this.tier = this.tier.parentTier;
        }

        if(this.vis.selected.length==0){
            this.toggleBackButton("invisible");
            return 0;
        }
    }

    // Only use the countries that are selected
    var vis = this.vis;
    var piechart = this;

    var parentTier = this.tier;
    var childTier = {'parentTier' : parentTier, 'current':[]};
    this.tier.childTier = childTier;

    var total = 0;


    var number_nonNull = d3.sum(this.data, function(d) {
        var val = +d[vis.selectedYear][piechart.serie];
        val = ($.isNumeric(val))?val:0;
        d.rounded = Math.round(val*100)/100;

        if (d.rounded > 0 && (vis.selected.indexOf(d.country) > -1) && parentTier.current.indexOf(d.country) > -1) {
            total += val;
            return 1;
        }
        else
            return 0;
    });

    var other_internet = 0;
    var threshold = this.threshold;
    var current_shown = 0;
    var minimum = this.minimumVisible;
    var nonNull_current = 0;

    localData.forEach(function (d, i) {

        var val = +d[vis.selectedYear][piechart.serie];
        val = ($.isNumeric(val))?val:0;

        //d.focusToggled = (vis.focused.indexOf(d.country)>-1);
        if(parentTier.current.indexOf(d.country) == -1){
            d.value = 0;
        }
        else {
            if((vis.selected.indexOf(d.country) > -1)){
                current_shown++;
                d.value = d.rounded;
            }
            else{
                d.value = 0;
            }
            // conditions for aggregating countries:
            if (current_shown > minimum+1 && val > 0 && total > 0 && val / total < threshold ) {
                other_internet += d.value;
                childTier.current.push(d.country);
                d.value = 0;
            }
            else {
                d.value = (vis.selected.indexOf(d.country) > -1) ? d.rounded : 0;
                if(d.value>0 && parentTier.current.indexOf(d.country) > -1 && childTier.current.indexOf(d.country) == -1) nonNull_current++;
            }
        }
    });

    if(number_nonNull > 0 && nonNull_current == 0)
    {
        this.zoomOut();
        return;
    }

   /* Removed for performance increase

   // See if there is reason to merge data (if there is only one country, there is no point
    var nonNull_other = d3.sum(this.data, function(d) {
        var val = +d[vis.selectedYear][piechart.serie];
        val = ($.isNumeric(val))?val:0;
        if (val > 0 && (vis.selected.indexOf(d.country) > -1) && childTier.current.indexOf(d.country) > -1) {
            return 1;
        }
        else
            return 0; });

    if(nonNull_other == 1){
        this.data.forEach(function(d){
            var val = +d[vis.selectedYear][piechart.serie];
            val = ($.isNumeric(val))?val:0;

            if(childTier.current.indexOf(d.country) > -1 && vis.selected.indexOf(d.country)>-1){
                d.value = Math.round(val*100)/100;
            }
        });
        other_internet = 0;
    }*/

    this.other.value = Math.round(other_internet*100)/100;

    return number_nonNull;
};

PieChart.prototype.update = function () {
    this.updateVisualization(this.updateData());
};

PieChart.prototype.updateVisualization = function(nonNull){
    var arc = this.getArc();
    var vis = this.vis;

    var arcTween = function (a) {
        var toggled = vis.focused.indexOf(a.data.country)>-1;
        var i = d3.interpolate(this._current, a);
        this._current = i(0);
        return function (t) {
            return arc(toggled)(i(t));
        };
    };

    var data = this.data;

    var nullarc = d3.svg.arc()
        .innerRadius(1)
        .outerRadius(1);

    var piechart = this;

    // Differentiate between there being data available or not:
    if(nonNull > 0) {

        this.path = this.path
            .data(this.pie(data)); // compute the new angles

        this.path
            .transition()
            .duration(750)
            .attrTween("d", arcTween) // redraw the arcs
            .each('end', function(d) {
                this._current = d;
            })
            .style('stroke', function(d,i){
                if(vis.focused.indexOf(d.data.country)>-1) return vis.selectorEnum.COUNTRY;
                return d3.rgb(piechart.color(d.data.country)).darker(1);
            })
            .style('stroke-width', function(d){
                return (vis.focused.indexOf(d.data.country)>-1)? 3 : 1;
            });
    }

    else {

        this.other.value = Math.max(this.other.value, 1);

        this.path
            .transition()
            .duration(400)
            .attr("d", function(d){ return nullarc(d)})
            .each(function(d) { this._current = d; });
    }
};

PieChart.prototype.getSort = function() {

    var vis = this.vis;
    var piechart = this;
    return function (a, b) {

        // make sure that aggregated data is always in last position
        if (a.country == "Other") return 1;
        if (b.country == "Other") return -1;

        else return (b[vis.selectedYear][piechart.serie] - a[vis.selectedYear][piechart.serie]);
    };
};

PieChart.prototype.visualize = function() {

    this.setRadius();
    var data = this.data;
    var vis = this.vis;
    var color = this.color;
    var piechart = this;

    // Sort the data once here, so that it doesn't affect anything later
    //data.sort(this.sort);

    this.pie = d3.layout.pie()
        .value(function(d) {return (d.country == "Other" || vis.selected.indexOf(d.country) > -1) ? d.value : 0; })
        .sort(piechart.getSort());

    var tooltip = d3.select("body").append("div")
        .attr("id", this.container + "-tooltip-pie")
        .attr("class", "tooltip")
        .style("opacity", 0)
        .style("position", "absolute")
        .style("z-index", "10")
        .style("opacity", 0);

    var arc = this.getArc();

    var pattern = vis.svg.append('defs')
        .append('pattern')
        .attr({ id:"stripes", width:"6", height:"6", patternUnits:"userSpaceOnUse", patternTransform:"rotate(20)"})
        .append("rect")
        .attr({ width:"4", height:"8", transform:"translate(0,0)", fill:"grey" });


    this.path = this.innerGraphic.selectAll('path')
        .data(this.pie(data))
        .enter()
        .append('path')
        .attr('d', function(d, i){
                return(arc());
        }())
        .each(function(d) { this._current = d; })
        .attr('fill', function(d, i) {
            if(d.data.country == "Other"){
                piechart.otherPath = this;
                return "url(#stripes)";//d3.rgb(200,200,200);
            }
            return color(d.data.country);
        })
        .style('stroke', function(d,i){
            if(vis.focused.indexOf(d.data.country)>-1) return vis.selectorEnum.COUNTRY;
            return d3.rgb(color(d.data.country)).darker(1);
        })
        .style('stroke-width', function(d){
            return (vis.focused.indexOf(d.data.country)>-1)? 3 : 1;
        })
        .attr("class", "clickable")
        .on("mouseover.tooltip", function(d){
            tooltip.html(
                d.data.country + "<br/>"  + piechart.getReadableValue(d.data.value))
                .transition()
                .duration(200)
                .style("opacity", .95);
        })
        .on("mousemove.tooltip", function(d){

            tooltip
                .style("left", d3.event.pageX + 10 + "px")
                .style("top", (d3.event.pageY - 10) + "px");

        })
        .on("mouseout.tooltip", function(d) {
            tooltip.transition()
                .duration(500)
                .style("opacity", 0);
        })
        // if you want the animation to work after clicking to increase the size properly (also after resizing)
        // You will have to take the code out of here and put it in a method that gets called using the right 'arc'
        // object
        .on("click", function(d){
                    if(d.data.country == "Other"){
                        piechart.zoomIn();
                    }
                    else{
                        piechart.toggleFocusCountry(d.data.country);
                        d3.select(this)
                            .style('stroke', function(b,i){
                                if(vis.focused.indexOf(d.data.country)>-1) return vis.selectorEnum.COUNTRY;
                                return d3.rgb(color(d.data.country)).darker(1);
                            })
                            .style('stroke-width', function(b){
                                return (vis.focused.indexOf(d.data.country)>-1)? 3 : 1;
                            })
                            .transition()
                            .duration(400);
            }
        });

    this.backButton = this.innerGraphic
        .append("path")
        .attr("d", d3.svg.symbol()
            .type('triangle-down')
            .size(function(d){return piechart.radius * piechart.radius / 9})
        )
        .attr("transform", "translate(" + - (piechart.radius/6 * (1 - Math.tan(Math.PI / 6))) + ",0) rotate(90)")
        .attr("fill", "lightgrey")
        .attr("class", "clickable")
        .attr("opacity", 0)
        .attr("visibility", "hidden")
        .on("click", function(d){
                piechart.zoomOut();
        })
        .on("mouseover", function (d) {
            d3.select(this)
                .transition()
                .duration(400)
                .attr("fill", "grey")})
        .on("mouseout", function (d) {
            d3.select(this)
                .transition()
                .duration(200)
                .attr("fill", "lightgrey")
        });

    this.resize();
};

