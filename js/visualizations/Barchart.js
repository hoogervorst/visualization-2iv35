/**
 * Created by sjhoo on 25-9-2015.
 */


var Barchart = function() {

    this.name = "Bar Chart";
    Visualization.apply(this, arguments);
    this.xLabelValue = "countries";
    this.resize();
    this.updateData();
};

Barchart.prototype = Object.create(Visualization.prototype);
Barchart.prototype.constructor = Barchart;


Barchart.prototype.selectYear = function(year){
    if(this.year == year && this.xLabelValue == "Countries") return;
    Visualization.prototype.selectYear.call(this, year);
    this.update(this.xLabelValue == "year" && (this.startYear != this.vis.yearRange[0] || this.endYear != this.vis.yearRange[1] || !this.yearCat));

};

Barchart.prototype.resize = function(){
    Visualization.prototype.resize.call(this);

    this.topPadding = this.height * 0.1;
    this.bottomPadding = this.height * 0.15;
    this.leftPadding = this.width * 0.15;
    this.rightPadding = this.width  * 0.05;

    if(this.bars) this.update();
};

Barchart.prototype.updateData = function(){

    this.localData = this.data.slice();

    var vis = this.vis;
    var barchart = this;

    this.localData = this.localData.filter(function(d){

        if (vis.selected.indexOf(d.country) == -1) return false;
        d.focusToggled = (vis.focused.indexOf(d.country)>-1);
        if(this.xLabelValue == "year") {
            return vis.focused.indexOf(d.country) > -1;
        }
        return true;
    });

    this.localData.sort(this.getSort());

};


Barchart.prototype.barchartCountries = function(xType){

    var result = {};
    var barchart = this;
    var padding = Math.max((barchart.width - 2 * barchart.leftPadding) / (barchart.localData.length * 10), 1);

    return function(d) {
        result.x = barchart.leftPadding + barchart.localData.indexOf(d) * ((barchart.width - barchart.leftPadding - barchart.rightPadding) / barchart.localData.length);
        result.width = (barchart.width - barchart.leftPadding - barchart.rightPadding) / barchart.localData.length - padding;
        return result[xType];
    }
};

Barchart.prototype.update = function(updateYearsView) {

    if(typeof(updateYearsView)==="undefined") updateYearsView = true;

    this.updateData();

    var barchart = this;
    var vis = this.vis;
    var data = barchart.localData;


    barchart.innerLabelText = (barchart.xLabelValue == "countries") ? barchart.vis.selectedYear :  barchart.vis.focused.filter(function(d){return vis.selected.indexOf(d) > -1}).length + " countr" + ((barchart.vis.focused.length==1)?"y":"ies");

    // Place the labels in the correct position
    this.xLabel
        .transition()
        .duration(400)
        .attr("y", barchart.height - barchart.bottomPadding)
        .attr("x", barchart.width / 2)
        .attr("dy", "2.5em")
        .style("text-anchor", "middle")
        .attr("class", "clickable")
        .text(barchart.xLabelValue);

    this.innerLabel
        .attr('class', function(){return (barchart.xLabelValue == "countries") ? 'inner-label h1' : 'inner-label h3'})
        .transition()
        .duration(400)
        .attr("y", barchart.topPadding)
        .attr("x", barchart.width - 2 * barchart.leftPadding)
        .attr("dx", "-2.5em")
        .text(barchart.innerLabelText);

    this.xLabel
        .on("click", function(){
            if(barchart.xLabelValue == "countries"){
                barchart.bars.remove();
                barchart.xLabelValue = "year";
            }
            else {
                barchart.yearCat.remove();
                barchart.bars = barchart.svg
                    .append("g")
                    .attr("class", 'country-bars')
                    .selectAll("rect");
                barchart.xLabelValue = "countries";
            }

            barchart.update();
        });


    // Set the axis and scales here

    if(barchart.xLabelValue == "countries") {
        this.y = d3.scale.linear()
            .domain([Math.min(0,d3.min(data,function(d){
                var val = +d[vis.selectedYear][barchart.serie];
                val = $.isNumeric(val)?val:0;
                return val;
            }))
                , d3.max(data, function (d) {
                    var val = +d[vis.selectedYear][barchart.serie];
                    val = $.isNumeric(val)?val:0;
                    return val;
            })])
    }
    else{
        this.y = d3.scale.linear()
            .domain([d3.min(data,function(d){
                if(barchart.vis.focused.indexOf(d.country)==-1 || barchart.vis.selected.indexOf(d.country) === -1) return 0;
                var min = 999999999999999;
                $.each(d, function(b, v){
                    if(+b >= barchart.vis.yearRange[0] && +b <= barchart.vis.yearRange[1]) {
                        var val = +v[barchart.serie];
                        val = $.isNumeric(val)?val:0;
                        min = Math.min(min, val);
                    }
                });
                return min;
            })
                , d3.max(data, function(d){
                if(barchart.vis.focused.indexOf(d.country)==-1 || barchart.vis.selected.indexOf(d.country) === -1) return 0;
                var max = -999999999999999;
                $.each(d, function(b, v){
                    if(+b >= barchart.vis.yearRange[0] && +b <= barchart.vis.yearRange[1]) {
                        var val = +v[barchart.serie];
                        val = $.isNumeric(val)?val:0;
                        max = Math.max(max, val);
                    }
                });
                return max;
            })])
    }

    this.y
        .range([this.height - this.bottomPadding, this.bottomPadding]);

    this.yAxis = d3.svg.axis()
        .scale(this.y)
        .orient("left")
        .tickFormat(this.getReadableValue)
        .ticks(11);
        //.tickFormat(d3.format(".1f2"));



    // From here on, manipulate the data to create the bars
    if(this.xLabelValue == "countries"){
        this.countryBars();
    }
    else{
        this.yearBars(updateYearsView);
    }

    this.yAxisElem
        .transition()
        .duration(400)
        .attr("transform", "translate("+ (barchart.leftPadding - 10) +",0)")
        .call(this.yAxis);


    this.xAxisElem
        .transition()
        .duration(400)
        .attr("transform", "translate(0,"+ (barchart.height - barchart.bottomPadding) +")")
        .style("opacity", function(){
            return (barchart.xLabelValue == "countries")?0:1
        })
        .call(this.xAxis);

    this.xAxisElem
        .selectAll('.tick')
        .attr('fill', function(d){
            return (d == vis.selectedYear)?Vis.selectorEnum.YEAR : "black";}
    )
        .classed("clickable", true)
        .on('click', function(d){
           vis.selectYear(d);
        });
};

Barchart.prototype.countryBars = function(){

    var barchart = this;
    var vis = this.vis;
    barchart.localData = barchart.localData.filter(function(d){
        var val = +d[vis.selectedYear][barchart.serie];
        val = ($.isNumeric(val))?val:0;
        return (val.toFixed(2) != 0);
    });

    var data = barchart.localData;

    this.bars = this.bars
        .data(data);

    this.bars
        .enter()
        .append("rect")
        .attr("y", barchart.height - barchart.bottomPadding)
        .attr("height", 0);

    this.bars
        .exit()
        .remove();


    this.bars
        .transition()
        .duration(400)
        .attr('x', barchart.barchartCountries("x"))
        .attr('y',function(d){ var val = d[vis.selectedYear][barchart.serie]; val = ($.isNumeric(val))?val:0; return barchart.y(val);})
        .attr("height", function(d){
            var v = d[vis.selectedYear][barchart.serie];
            v = $.isNumeric(v)?v:0;
            return barchart.height - barchart.bottomPadding - barchart.y(v);
        })
        .attr("width", barchart.barchartCountries("width")) ;

    this.bars
        .attr('fill', function(d){return barchart.color(d.country);})
        .style('stroke', function(d,i){
            if(vis.focused.indexOf(d.country)>-1) return vis.selectorEnum.COUNTRY;
            return d3.rgb(barchart.color(d.country)).darker(1);
        })
        .style('stroke-width', function(d){
            return (vis.focused.indexOf(d.country)>-1 && d.value > 0)?3:0;
        })
        .attr("class", "clickable")
        .style('stroke', function(d){
            if(vis.focused.indexOf(d.country)>-1) return vis.selectorEnum.COUNTRY;
            return d3.rgb(barchart.color(d.country)).darker(1);
        })
        .style('stroke-width', function(d){
            return (vis.focused.indexOf(d.country)>-1)? 3 : 0;
        })
        .on("mouseover.tooltip", function(d){
            barchart.tooltip.html(
                d.country + "<br/>"  + barchart.getReadableValue(+d[vis.selectedYear][barchart.serie]))
                .transition()
                .duration(200)
                .style("opacity", .95);
        })
        .on("mousemove.tooltip", function(d){

            barchart.tooltip
                .style("left", d3.event.pageX + 10 + "px")
                .style("top", (d3.event.pageY - 10) + "px");

        })
        .on("mouseout.tooltip", function(d) {
            barchart.tooltip.transition()
                .duration(500)
                .style("opacity", 0);
        })
        .on("mouseover", function (d) {
            d3.select(this.parentNode.appendChild(this))
                .style('stroke-width', 3)
                .transition()
                .duration(400);
        })
        .on("mouseout", function (d) {
            d3.select(this)
                .style('stroke-width', function(b){
                    return (vis.focused.indexOf(d.country)>-1)?3:0;
                })
                .transition()
                .duration(400)
        })
        .on("click", function(d){
            barchart.toggleFocusCountry(d.country);
        });
};

Barchart.prototype.yearBars = function(currentYearChanged){


    var barchart = this;
    var vis = this.vis;
    var localdata = barchart.localData.filter(function(d){ return vis.focused.indexOf(d.country)> -1 ;});


    this.startYear = barchart.vis.yearRange[0];
    this.endYear = barchart.vis.yearRange[1];

    var years = [];
    for(var i = +this.startYear; i <= +this.endYear; i++){ years.push(i);}


    this.x = d3.scale.ordinal()
        .rangeRoundBands([this.leftPadding, this.width - this.rightPadding], 0.1);

    this.x2 = d3.scale.ordinal();

    var data = [];

    // for each focused country
    localdata.forEach(function(d) {
        // Add the selected years to the data
        years.forEach(function(i,v){
            if(!data[v]) data[v] = {year: +i, internetByYears : []};
            var val  = $.isNumeric(+d[i][barchart.serie])?+d[i][barchart.serie]:0;
           data[v].internetByYears.push({country: d.country, value: val, year:+i});
        });
    });


    // set the scales to be per country and per block
    this.x
        .domain(years);
    this.x2
        .domain(vis.focused)
        .rangeRoundBands([0, this.x.rangeBand()], 0.02);


    if(currentYearChanged || !this.yearCat) {
        if (this.yearCat) this.yearCat.remove();

        this.yearCat = this.years
            .data(data)
            .enter().append("g")
            .attr("class", "g")
            .attr("transform", function (d) {
                return "translate(" + barchart.x(d.year) + ",0)";
            });

        this.yearCat.selectAll("rect")
            .data(function (d) {
                return d.internetByYears;
            })
            .enter().append("rect")
            .attr("width", this.x2.rangeBand())
            .attr("x", function (d) {
                return barchart.x2(d.country);
            })
            .attr("y", barchart.height - barchart.bottomPadding)
            .attr("height", 0)
            .transition()
            .duration(400)
            .attr("y", function (d) {
                return barchart.y(d.value);
            })
            .attr("height", function (d) {
                return barchart.height - barchart.bottomPadding - barchart.y(d.value)
            });

        this.yearCat.selectAll("rect")
            .style("fill", function (d) {
                return barchart.color(d.country);
            })
            .style('stroke', function (d, i) {
                if (vis.focused.indexOf(d.country)>-1) return vis.selectorEnum.COUNTRY;
                return d3.rgb(barchart.color(d.country)).darker(1);
            })
            .style('stroke-width', function (d) {
                return (vis.focused.indexOf(d.country)>-1 && d.value > 0) ? 3 : 0;
            })
            .attr("class", "clickable")
            .style('stroke', function (d) {
                if (d.focusToggled) return "#ffd700";
                return d3.rgb(barchart.color(d.country)).darker(1);
            })
            .style('stroke-width', 0)
            .on("mouseover.tooltip", function (d) {
                barchart.tooltip.html(
                    d.country + "<br/>" + d.year + "<br/>" + (+d.value).toFixed(2))
                    .transition()
                    .duration(200)
                    .style("opacity", .95);
            })
            .on("mousemove.tooltip", function (d) {

                barchart.tooltip
                    .style("left", d3.event.pageX + 10 + "px")
                    .style("top", (d3.event.pageY - 10) + "px");

            })
            .on("mouseout.tooltip", function (d) {
                barchart.tooltip.transition()
                    .duration(500)
                    .style("opacity", 0);
            })
            .on("mouseover", function (d) {
                d3.select(this.parentNode.appendChild(this))
                    .style('stroke-width', 3)
                    .transition()
                    .duration(400);
            })
            .on("mouseout", function (d) {
                d3.select(this)
                    .style('stroke-width', 0)
                    .transition()
                    .duration(400)
            });

        // set the real height and grow towards it
        this.yearCat.selectAll("rect")

    }

    this.xAxis = this.xAxis
        .scale(this.x)
        .orient("bottom")
        .tickFormat(d3.format("d"));



};

Barchart.prototype.visualize = function() {

    this.tooltip = d3.select("body").append("div")
        .attr("id", this.container + "-tooltip")
        .attr("class", "tooltip")
        .style("opacity", 0)
        .style("position", "absolute")
        .style("z-index", "10")
        .style("opacity", 0);


    this.bars = this.svg
        .append("g")
        .attr("class", 'country-bars')
        .selectAll("rect");

    this.years = this.svg
        .append("g")
        .attr("class", 'year-bars')
        .selectAll(".yearCat");

    this.xAxis = d3.svg.axis();

    this.yAxisElem = this.svg
        .append("g")
        .attr("class", "y axis");

    this.xAxisElem = this.svg
        .append("g")
        .attr("class", "x axis")
        .attr("transform", "translate("+this.leftPadding + ","+ (this.height - this.bottomPadding * 0.9) +")")
        .style("opacity", 0);

    this.xLabel = this.svg
        .append('g')
        .attr('class', 'axis-label h4')
        .append("text");

    this.innerLabel = this.svg
        .append('g')
        .style("text-anchor", "right")
        .append("text")
        .text(this.selectedYear);

    this.update();
};