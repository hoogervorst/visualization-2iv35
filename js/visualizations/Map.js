/**
 * Created by sjhoo on 20-10-2015.
 */

var Map = function() {

    this.name = "Map";
    this.currentProjection = 'Mercator';
    this.currentMethod = 'Map';
    Visualization.apply(this, arguments);
    this.resize();
};

Map.prototype = Object.create(Visualization.prototype);
Map.prototype.constructor = Map;


Map.prototype.selectYear = function(year){
    Visualization.prototype.selectYear.call(this, year);
};

Map.prototype.resize = function(){
    Visualization.prototype.resize.call(this);

    this.innerGraphic.attr('width', this.width)
        .attr('height', this.height);

    this.scale = (this.width - 1) / 2 / Math.PI;
};

Map.prototype.selectYear = function(year){
    Visualization.prototype.selectYear.call(this, year);
    this.update();
    $(this.container+"-year").text(this.year);
};

Map.prototype.initializeVisualization = function() {

    this.projections = {
        'Mercator' :   d3.geo.mercator,
        'Orthographic': d3.geo.orthographic
    };

    Visualization.prototype.initializeVisualization.call(this);

    var map = this;
    this.zoom = d3.behavior.zoom()
        .scale(this.scale);

    $(this.container + "-svg")
        .prepend('<div class="clearfix map-top">'+
        '<span id="'+this.container.slice(1) + '-year" class="h1 no-margin pull-left col-xs-4 piechart">'+this.year + '</span>'+
        //'<div class="col-xs-4">'+
            '<div class="btn-group col-xs-4">'+
                    '<button id="'+ this.container.slice(1) +'-projection" class="btn btn-default btn-block wrap"' +
                        'data-toggle="dropdown" aria-haspopup="true">'+ this.currentProjection +  ' <span ' +
                        'class="caret"></span>' +
                    '</button>' +
                    '<ul class="dropdown-menu" style="z-index:999">' +
                        '<li projection="Mercator"><a href="#" >Mercator</a></li>' +
                        '<li projection="Orthographic"><a href="#" >Orthographic</a></li>' +
                    '</ul>' +
                '</div>'+
            '<div class="btn-group col-xs-4">'+
                    '<button id="'+ this.container.slice(1) +'-method" class="btn btn-default btn-block wrap">'+ this.currentMethod +
                    '</button>' +
            '</div>'+
        '</div>');

    var projection_selector = $(this.container+"-projection");
    var method_selector = $(this.container+"-method");

    projection_selector.siblings('ul').children('li').click(function(d){
        map.currentProjection = $(this).attr('projection');
        map.setProjection();
        map.update();
        projection_selector.html(map.currentProjection +  ' <span class="caret"></span>');

    });
    method_selector.click(function(){

        if(map.currentMethod == "Map")
            map.currentMethod = "Chloropleth";
        else
            map.currentMethod = "Map";

        method_selector.text(map.currentMethod);
        map.update();
    });

};

Map.prototype.setProjection = function (){

    switch(this.currentProjection) {
        case "Mercator" :
            this.projection = d3.geo.mercator()
                .precision(5);
            this.scale = (this.width - 1) / 2 / Math.PI;
            this.zoom.translate([0, this.height/8]);
            this.zoom.center(null);
            this.innerGraphic.on('.drag', null);
            this.innerGraphic.call(this.zoom);
            break;

        case "Orthographic" :

            this.projection = d3.geo.orthographic()
                .scale(this.scale)
                .clipAngle(90)
                .precision(5);
            this.scale = (this.width - 1) / Math.PI;
            this.zoom.translate([0,0]);
            this.zoom.center([0,0]);
            this.innerGraphic.on("mousedown.zoom", null)
                .on("touchstart.zoom", null)
                .on("touchmove.zoom", null)
                .on("touchend.zoom", null);
            this.innerGraphic.call(this.getOrthographicDrag());
            break;
    }

    this.zoom
        .scale(this.scale);
    return this.projection;
};

Map.prototype.getOrthographicDrag = function() {
    var map = this;
    var sens = 0.25;

    return d3.behavior.drag()
        .origin(function() { var r = map.projection.rotate(); return {x: r[0] / sens, y: -r[1] / sens}; })
        .on("drag", function() {
            var lambda = d3.event.x * sens,
                phi = -d3.event.y * sens,
                rotate = map.projection.rotate();
            //Restriction for rotating upside-down
            phi = phi > 90 ? 90 :
                phi < -90 ? -90 :
                    phi;
            map.projection.rotate([lambda, phi]);
            map.update();
        });
};

Map.prototype.setPath = function(){

    this.realpath = d3.geo.path()
        .projection(this.projection);
    return this.realpath;
};

Map.prototype.getZoom = function(){

    this.zoom
        .scaleExtent([0.5 * this.scale, 15 * this.scale])
        .on("zoom", this.getUpdate());

    return this.zoom;
};

Map.prototype.getUpdate = function() {
    var data = this.vis.data;
    var vis = this.vis;
    var color = this.color;
    var chloropleth = d3.scale.linear()
        .domain([this.vis.series[this.serie].min, Math.ceil(this.vis.series[this.serie].max/10)*10])
        .range(["white", "red"]);

    var method = this.currentMethod;
    var map = this;


    return function () {

        var t = map.zoom.translate(),
            s = map.zoom.scale();

        map.projection
            .translate(t)
            .scale(s);

        var realpath = map.setPath();

        map.innerGraphic.selectAll('.sphere')
            .attr('d', realpath);

        map.innerGraphic.selectAll('path.country')
            .attr('d', function (d) {
                if(d.geometry) return realpath(d.geometry);
            })
            .attr("class", function(d){
                return ("country " + ((vis.selected.indexOf(d.country)>-1) ? "clickable" : ""));
            })
            .style('opacity', function(d){
                if(method == "Chloropleth" || vis.selected.indexOf(d.country)>-1) {
                    return 1;
                }
                return 0.3;
            })
            .style('stroke', function (d, i) {
                if (vis.focused.indexOf(d.country)>-1) return vis.selectorEnum.COUNTRY;
                return d3.rgb(color(d.country)).darker(1);
            })
            .style('stroke-width', function (d) {
                return (vis.focused.indexOf(d.country)>-1) ? 3 : 1;
            });

        map.innerGraphic.selectAll('path.country')
            .transition()
            .duration(400)
            .attr('fill', function(d, i) {
                switch (method){
                    case "Map" : return color(d.country); break;
                    case "Chloropleth" : return ( vis.selected.indexOf(d.country)==-1)?"lightgray":chloropleth(d[vis.selectedYear][map.serie]); break;
                }
            });
    };
};

Map.prototype.update = function(){
    this.getZoom();
    this.getUpdate()();
};

Map.prototype.visualize = function(){
    var data = this.vis.data;
    var vis = this.vis;
    var color = this.color;
    var map = this;

    var projection = this.setProjection();
    var realpath = this.setPath();

    // Add tooltip for map elements
    var tooltip = d3.select("body").append("div")
        .attr("id", this.container + "-tooltip-map")
        .attr("class", "tooltip")
        .style("opacity", 0)
        .style("position", "absolute")
        .style("z-index", "10")
        .style("opacity", 0);

    this.background = this.innerGraphic
        .append("path")
        .datum({type: "Sphere"})
        .attr("class", "sphere")
        .style('opacity', 1)
        .style('fill', "white")
        .attr("d", realpath);

    this.path = this.innerGraphic.selectAll('path.country')
        .data(data)
        .enter()
        .append('path')
        .each(function(d) { this._current = d; })
        .attr("class", function(d){
            return ("country " + ((vis.selected.indexOf(d.country)>-1) ? "clickable" : ""));
        })
        .on("mouseover.tooltip", function(d){
            tooltip.html(
                d.country + "<br/>"  + map.getReadableValue(+d[vis.selectedYear][map.serie]))
                .transition()
                .duration(200)
                .style("opacity", .95);
        })
        .on("mousemove.tooltip", function(d){
            tooltip
                .style("left", d3.event.pageX + 10 + "px")
                .style("top", (d3.event.pageY - 10) + "px");

        })
        .on("mouseout.tooltip", function(d) {
            tooltip.transition()
                .duration(500)
                .style("opacity", 0);
        })
        .on("click", function(d) {
            if(vis.selected.indexOf(d.country)>-1) {
                map.toggleFocusCountry(d.country);
                map.update();
            }
        })
        .on("mouseover", function (d) {
            if(vis.selected.indexOf(d.country)>-1) {
                d3.select(this.parentNode.appendChild(this))
                    .style('stroke-width', 3)
                    .transition()
                    .duration(400);
            }
        })
        .on("mouseout", function (d) {
            d3.select(this)
                .style('stroke-width', function(b){
                    return (vis.focused.indexOf(d.country)>-1)?3:1;
                })
                .transition()
                .duration(400)
        });

    this.update();


};
